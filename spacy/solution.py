count_entity_labels = """
def count_entity_labels(review):
    entity_label_counts = {}
    parsed_review = nlp(review)
    for entity in parsed_review.ents:
        elc = entity_label_counts.get(entity.label_, 0)
        entity_label_counts[entity.label_] = elc + 1
    return entity_label_counts

parsed_reviews.loc[:, 'entity_label_counts'] = parsed_reviews.loc[:, "review"].apply(count_entity_labels)
parsed_reviews
"""