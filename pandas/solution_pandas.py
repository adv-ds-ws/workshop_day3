age_smaller_50 = """
life_expectancy[life_expectancy.index < 50]
"""

select_2_5 = """
life_expectancy.iloc[[2, 5]]
"""

select_20_30 = """
life_expectancy.loc[[20, 30]]
"""

increase_expected_age = """
life_expectancy['age_at_death_m'] = life_expectancy.index + life_expectancy['life_expec_m']
life_expectancy['age_at_death_f'] = life_expectancy.index + life_expectancy['life_expec_f']
life_expectancy.diff(1).mean()
"""


get_life_expectancy_1 = """
def get_life_expectancy_1(age):
    if age >= 100:
        return life_expectancy.loc[100]
    if age < 0:
        return life_expectancy.loc[0]
    if age % 10 == 0:
        return life_expectancy.loc[age]
    
    lower_age = int(age / 10) * 10
    lower_data = life_expectancy.loc[lower_age]

    upper_age = int(age / 10) * 10 + 10
    upper_data = life_expectancy.loc[upper_age]
    
    return lower_data + ((upper_data - lower_data) / (upper_age - lower_age)) * (age - lower_age)
get_life_expectancy_1(25)
"""

get_life_expectancy_2 = """
def get_life_expectancy_2(age):
    return life_expectancy.reindex(range(101)).interpolate().loc[age]
get_life_expectancy_2(25)
"""

get_life_expancy_for_people = """
people_and_life_expectancy = people.merge(life_expectancy, left_on="Age", right_on="age")
people.loc[people['Male'], 'life_expec'] = people_and_life_expectancy['life_expec_m']
people.loc[~people['Male'], 'life_expec'] = people_and_life_expectancy['life_expec_f']
people
"""

ads_lowest_open = """
prices_df[['Date', 'symbol', 'Open']]\\
    [prices_df.symbol == 'ADS']\\
    .sort_values('Open')\\
    .head(3)
"""

mrk_highest_close = """
prices_df[['Date', 'symbol', 'Close']]\\
    [prices_df["symbol"] == 'MRK']\\
    .sort_values('Close', ascending=False)\\
    .head(2)
"""

min_max_close = """
prices_df[['Close', 'symbol']]\\
    .groupby('symbol')\\
    .agg([np.max, np.min])
"""

avg_change_of_price = """
prices_df['delta'] = prices_df.Close - prices_df.Open

prices_df[['symbol', 'delta']]\\
    .groupby('symbol')\\
    .mean()
"""

max_close_date_1 = """
prices_df[['symbol', 'Date', 'Close']]\\
    .sort_values(['symbol', 'Close', 'Date'], ascending=False)\\
    .groupby('symbol')\\
    .head(1)
"""

max_close_date_2 = """
prices_df.iloc[prices_df\\
    .groupby(['symbol'])\\
    ['Close']\\
    .idxmax()][['symbol', 'Date', 'Close']]
"""

pivot_volume = """
prices_df.pivot_table(index="year_mon", columns="symbol", values="Volume", aggfunc=np.sum)
"""
