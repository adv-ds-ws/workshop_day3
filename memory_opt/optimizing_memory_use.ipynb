{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Optimizing Memory Use\n",
    "\n",
    "There are two typical bottlenecks for developers:\n",
    "* insufficient memory\n",
    "* insufficient processing power\n",
    "\n",
    "\n",
    "While insufficient processing power usually makes a program run slow, insufficient memory can prevent me from running it at all. \n",
    "For this reason, we treat memory optimization first. Performance optimization is treated separately. Often less memory intensive solutions also run faster.\n",
    "\n",
    "## Workflow for memory optimization:\n",
    "0. State your goal\n",
    "1. Measure\n",
    "2. Optimize\n",
    "3. Measure\n",
    "4. If goal is reached you are done. Else: goto 2.\n",
    "\n",
    "## Profiling:\n",
    "Measuring (i. e. profiling) is crucial. As in performance optimization, we don't want to waste time by optimizing code that may be suboptimal but has little impact on our metric.\n",
    "\n",
    "\n",
    "Be warned that profiling is seldom an exact science. There is always an overhead for measuring plus we don't always measure quite the correct thing. For example: Just because 4GB of memory are free doesn't mean that the 4GB are contiguous. Our free memory may be fragmented. Hence, we may not be able to fit a one big file inside. \n",
    "\n",
    "\n",
    "*Example:*\n",
    "My total memory use is 7GB. I optimize a data structure to use 1/10 of the memory it used before. Did I win? That depends on how much memory this structure used before. If it used 10MB I did not gain anything. If it used 3GB it is a different story.\n",
    "\n",
    "\n",
    "*Remember the words of Donald Knuth:*\n",
    "> We should forget about small efficiencies, say about 97% of the time: \n",
    "> premature optimization is the root of all evil.\n",
    "\n",
    "\n",
    "Note that he is **not** saying \"don't optimize\"!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*if you need quick reminder on how to use jupyter (formerly called Ipython):* `%quickref`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%quickref"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import glob\n",
    "import time\n",
    "import pandas as pd\n",
    "import numpy as np \n",
    "import datetime"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fibonacci(i_fibo):\n",
    "    if i_fibo < 3:\n",
    "        return 1\n",
    "    n1, n2 = 1, 1\n",
    "    for i in range(2, i_fibo):\n",
    "            n1, n2 = n2, n1+n2\n",
    "    return n2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tools for general memory profiling\n",
    "\n",
    "`psutil` can tell me how much memory I have (available)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import psutil\n",
    "psutil.virtual_memory()  # byte / (1024.0 ** 3) = GB"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`pympler` is a good tool for general memory profiling:\n",
    "\n",
    "https://pythonhosted.org/Pympler/\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pympler\n",
    "from pympler import tracker"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We first need to create a tracker:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "memory_tracker = tracker.SummaryTracker()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tracker can then be used to tell us how of memory is used:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "memory_tracker.print_diff()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see how our memory use changes after creating a big list!\n",
    "\n",
    "Before you check, take a guess: What do you think how much more memory should be occupied by what data type?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "memory_wasting_list = list(range(1000000))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "memory_tracker.print_diff()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Short Excursion: Performance profiling\n",
    "\n",
    "Since we are at it, we will take a quick glimpse at how to measure performance (the time our program is taking)!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The easiest way in jupyter is using the `%%timeit` cellmagic:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "[i**3 for i in range(1000000)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are not working in Jupyter, there is nothing wrong with simply taking the time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "time_start = datetime.datetime.now()\n",
    "time.sleep(2) # Do stuff!\n",
    "time_end = datetime.datetime.now()\n",
    "time_taken = (time_end - time_start).seconds\n",
    "print(time_taken)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a better breakdown of what took how long we can use `%%prun -D program.prof`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%prun -D program.prof\n",
    "for i in range(3):\n",
    "    time.sleep(1)\n",
    "    [i**3 for i in range(1000000)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An even nicer breakdown that visualizes the methods that call each other is `snakeviz`. \n",
    "\n",
    "**Warning**: To use `snakeviz` in the easy way we do here, online connection is required. See the documentation on how to do it offline."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext snakeviz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%snakeviz\n",
    "files = glob.glob('*.txt')\n",
    "for file in files:\n",
    "    with open(file) as f:\n",
    "        print(hashlib.md5(f.read().encode('utf-8')).hexdigest())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Resources for Profiling:\n",
    "\n",
    " * http://pynash.org/2013/03/06/timing-and-profiling/\n",
    "\n",
    " * https://jiffyclub.github.io/snakeviz/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optimizing memory use in pandas /  numpy\n",
    "\n",
    "While it is good to know the general tools for profiling in python, we won't need them for memory profiling in pandas an numpy. Those packages come with their own handy tools!\n",
    "\n",
    "A reference for the basic data types in numpy (and hence in pandas):\n",
    "\n",
    "https://docs.scipy.org/doc/numpy-1.13.0/user/basics.types.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create an example dataframe forn a numpy array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_rand_string_date():\n",
    "    start = datetime.datetime(2018,1,1)\n",
    "    d = start + datetime.timedelta(days=np.random.randint(1, 700))\n",
    "    return d.strftime(\"%Y-%m-%d\")\n",
    "\n",
    "rows = 500000\n",
    "np.random.seed(443)\n",
    "patients = np.array(\n",
    "    [range(rows), \n",
    "     np.random.choice([\"male\", \"female\"], rows), \n",
    "     np.random.choice([\"A\", \"B\", \"AB\", \"O\"], rows), \n",
    "     np.random.randint(1, 140, rows),\n",
    "     np.random.rand(rows)*10e4 + np.random.randn(rows),\n",
    "     [make_rand_string_date() for row in range(rows)]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df = pd.DataFrame(patients.T, columns = [\"ID\", \"sex\", \"blood\", \"age\", \"npv\", \"next_visit\"])\n",
    "patients_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get the true size of \"object\" columns, we need to specify that we want deep memory usage. \n",
    "We can either get a lot of information about the dataframe with  `df.info(memory_usage='deep')` or get just the memory size with `df.memory_usage(deep=True)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df.info(memory_usage='deep') \n",
    "print(\"\") \n",
    "patients_df.memory_usage(deep=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A numpy array can only have one datatype for all columns. It is essentially a Matrix for calculations.\n",
    "If we store strings in one column of an array, all columns will be treated as string. Since we created our dataframe from such an array, it consists of strings.\n",
    "\n",
    "Strings get represented as the `object` datatype since strings themselves are not basic building blocks like numbers or bools but are themselves lists of characters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will first take a look how much memory is needed to store the patients age."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.DataFrame(patients_df[\"age\"]).info(memory_usage='deep')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we are content to store patients age as a whole number, we can conserve  memory by casting it as integer. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df[\"age\"] = patients_df[\"age\"].astype(int) \n",
    "pd.DataFrame(patients_df[\"age\"]).info(memory_usage='deep')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " - How old can humans get? \n",
    " - How big does my integer need to be? \n",
    "\n",
    "Review the datatypes to determine which is appropriate:\n",
    "https://docs.scipy.org/doc/numpy-1.13.0/user/basics.types.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df[\"age\"] = patients_df[\"age\"].astype(np.int16) \n",
    "pd.DataFrame(patients_df[\"age\"]).info(memory_usage='deep')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Does the integer need to be signed or unsigned?\n",
    "\n",
    "*Hint: Can age be negative?*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df[\"age\"] = patients_df[\"age\"].astype(np.uint8) \n",
    "pd.DataFrame(patients_df[\"age\"]).info(memory_usage='deep')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code below casts  numbers from -1 to 258 as `np.uint8`. \n",
    "What output do you expect?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "pd.DataFrame([-1,0,1,254,255,256,258]).astype(np.uint8) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see how our optimization affected or dataframe so far:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "patients_df.info(memory_usage='deep')\n",
    "patients_df.memory_usage(deep=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let's see how sex is affecting or memory usage:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df[\"sex\"].nbytes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is the best way to encode sex (or variables like it)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_2 = patients_df.rename(index=str, columns={\"sex\": \"is_male\"})\n",
    "patients_df_2[\"is_male\"] = patients_df_2[\"is_male\"] == \"male\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_2[\"is_male\"].nbytes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above type of encoding is called \"one-hot\". We transfer a categorical column to one more boolean columns. Make sure to choose one column less than you have categories. This means if you encode sex as \"is_male\", you don't need an additional \"is_female\" column, since this information is redundant. One hot encoding is often chosen for machine learning applications. For other cases, categories represented by integers also work well. Just don't use strings (if you want to save memory), since they take up a lot of memory. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now try to reduce the size of blood type. What is the correct way to store blood type information?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_3 = patients_df_2.copy()\n",
    "patients_df_3[\"blood\"] = patients_df_3[\"blood\"].astype(\"category\")\n",
    "# Alternative way: pd.Categorical(patients_df_3[\"blood\"], set(patients_df_3[\"blood\"]))\n",
    "patients_df_3.head()[\"blood\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can take a look at how the blood types are now encoded internally:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_3.head()[\"blood\"].cat.codes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, internally `int8` is used, which is much more parsimonious than string!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_3.memory_usage(deep=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you need to convert it back, simply cast it as a string again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_3.head()[\"blood\"].astype(str)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let's save mony on the index. This is a bit tricky, since it depends on how large we expect our index to grow. In any case, our index won't get negative, so let's use `uint16`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_3[\"ID\"] = patients_df_3[\"ID\"].astype(np.uint16) # Maybe too small, but in any case unsigned is right!\n",
    "patients_df_3.info(memory_usage='deep')\n",
    "patients_df_3.memory_usage(deep=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_3[\"npv\"].head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Questions**:\n",
    "1. How big is the largest ID we can store now? \n",
    "2. By what factor would memory use go down if we used `int8` instead of `uint16`? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lastly, we convert npv (net present value of the patient). It is still taking up a lot of space, since it is stored as string.\n",
    "Npv can grow quite big in our sample (we are treating the super rich or maybe this is in venzuelan currency):\n",
    "\n",
    "Let's first convert it to the largest float, which is what numbers are normally represented as in pandas / numpy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_4 = patients_df_3.copy()\n",
    "patients_df_4[\"npv\"] = patients_df_4[\"npv\"].astype(np.float64) \n",
    "patients_df_4.info(memory_usage='deep')\n",
    "print(patients_df_4.memory_usage(deep=True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That helped a lot! But it was string before, so that was an easy fix. Can we do better?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_5 = patients_df_4.copy()\n",
    "patients_df_5[\"npv\"] = patients_df_5[\"npv\"].astype(np.float32) \n",
    "patients_df_5.memory_usage(deep=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Saved 50% of memory usage. But did we loose precision?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dev = max(abs(patients_df_5[\"npv\"] - patients_df_4[\"npv\"]))\n",
    "print(\"Deviation: {}\".format(dev))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The deviation is quite small! The number above is the maximum deviation. \n",
    "Lets look at the Minimum an Maximum before and after conversion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(patients_df_4[\"npv\"].min(), patients_df_4[\"npv\"].max())\n",
    "print(patients_df_5[\"npv\"].min(), patients_df_5[\"npv\"].max())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looks good!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's take a look at our now smaller dataframe! Still pretty much the same information in a smaller package:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_5.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_5.info(memory_usage='deep')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last memory intensive column  in our dataframe is the date in string format. Let's parse it to a proper datetime object!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_5[\"next_visit\"] = pd.to_datetime(patients_df_5[\"next_visit\"], format=\"%Y-%m-%d\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_5.info(memory_usage='deep')\n",
    "patients_df_5.memory_usage(deep=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we reduced the size significantly!\n",
    "\n",
    "For a fair comparison, lets assume we did not create the dataframe from a string-array but rather build it as a dataframe with automatically allocated datatypes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_autotypes = pd.DataFrame({\n",
    "    \"ID\": range(rows), \n",
    "    \"sex\": np.random.choice([\"male\", \"female\"], rows), \n",
    "    \"blood\": np.random.choice([\"A\", \"B\", \"AB\", \"O\"], rows),\n",
    "    \"age\": np.random.randint(1, 140, rows), \n",
    "    \"npv\": np.random.rand(rows)*10e4 + np.random.randn(rows), \n",
    "    \"next_visit\": [make_rand_string_date() for row in range(rows)]}\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_autotypes.info(memory_usage='deep')\n",
    "patients_df_autotypes.memory_usage(deep=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We managed to pack the same information in 38% of the memory! \n",
    "\n",
    "Not bad!\n",
    "\n",
    "Depending of your use case you may get even better results depending on you type of data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Efficiently reading data from csv\n",
    "In general, CSVs can be messy! But pandas comes with a **lot** of options and the documentation is quite good:\n",
    "\n",
    "https://pandas.pydata.org/pandas-docs/version/0.18/generated/pandas.read_csv.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets write our csv first:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_df_autotypes.iloc[:1000,:].to_csv(\"patients.csv\", index=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We don't have to read in the whole file at once:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_short = pd.read_csv(\"patients.csv\", nrows=20)\n",
    "patients_short.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can determine from the short datatset which columns we need and what datatye each column should be. Then we just read in the data we really need. This way we may be able to read a csv file, with a size which exceeds our RAM size!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "col_names = ['id', 'sex', 'blood_type', 'age', 'net_pres_val', 'next_visit_date'] # We can rename!\n",
    "cols_we_want = ['id', 'blood_type', 'age', 'next_visit_date'] # Only those go in the dataframe\n",
    "dtype = {'id': 'uint16', \n",
    "          'blood_type': 'category', \n",
    "          'age':'uint8'} # We only need to specify those we want!\n",
    "parse_dates = ['next_visit_date'] # Automatically parsed by pandas!\n",
    "patients_subset = pd.read_csv(\"patients.csv\", names=col_names, usecols=cols_we_want, \n",
    "                              dtype=dtype, parse_dates=parse_dates, skiprows=1)\n",
    "patients_subset.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "patients_subset.info(memory_usage='deep')\n",
    "patients_subset.memory_usage(deep=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What if my data is still too big?\n",
    "You may still be able to read it, if you only need *some* values. In this case you can read the data in chunks and filter them before writing them to csv.\n",
    "\n",
    "To this end, we create an iterator which we can use to go over the data in chunks of 100. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "iter_csv = pd.read_csv(\"patients.csv\", iterator=True, chunksize=100) # Chunksize can be choosen according to available memory!\n",
    "    \n",
    "patients_female = pd.concat(\n",
    "    [chunk[chunk['sex']=='female'] for chunk in iter_csv]\n",
    ")\n",
    "patients_female.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## WHAT IF MY DATA IS STILL TOO BIG???\n",
    "\n",
    "You can get fancy with doing operations in chunks. The problem is: Doing things like joins in chunks is a lot of work.\n",
    "Luckily this type of system already exists with technologies that use the map-reduce algorithm. Hadoop is the most well known. Apache Spark builds on Hadoop and is easier to use for pandas-like operations. \n",
    "There is a python implementation of Spark: pySpark.\n",
    "\n",
    "Spark allows you do scale your calculations over multiple machines and it uses the disk to cache results.\n",
    "It is also too broad a topic to quickly cover here. It deserves it's own course.\n",
    "\n",
    "https://spark.apache.org/docs/0.9.0/quick-start.html"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
