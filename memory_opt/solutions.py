sol_optimization = """
print(f"initial memory usage of the DataFrame:\t\t{game_df.memory_usage(index=True, deep=True).sum()/10**6} MB")

# Small and positive numeric values can be efficently stored as uint8
cols_to_uint8 = [
    'away_Goal',
    'home_Goal',
    'away_FoulsCommited',
    'home_FoulsCommited',
    'away_ShotsOnTarget',
    'home_ShotsOnTarget',
    'away_ShotsTotal',
    'home_ShotsTotal'
    ]
game_df[cols_to_uint8] = game_df[cols_to_uint8].astype("uint8")

# Our float values only have a precision of 2 decimal points. We can therefore save them as "float16"
cols_to_float16 = [
    'away_PassSuccess',
    'home_PassSuccess',
    ]
game_df[cols_to_float16] = game_df[cols_to_float16].astype("float16")

# columns which have a bounded number of elements can be efficiently saved as "categorical" values
cols_to_category = [
     'away_Formation',
     'home_Formation',
     'away_Team',
     'home_Team',
     'away_ManagerName',
     'home_ManagerName',
     'venueName',
     'division'
    ]

game_df[cols_to_category] = game_df[cols_to_category].astype("category")

print(f"memory usage after type changing:\t\t{game_df.memory_usage(index=True, deep=True).sum()/10**6} MB")

cols_keep =  ['date',
    'away_Team',
    'home_Team',
    'away_Goal',
    'home_Goal',
    'away_FoulsCommited',
    'home_FoulsCommited',
    'venueName',
    'away_ShotsTotal',
    'home_ShotsTotal',
    'away_ShotsOnTarget',
    'home_ShotsOnTarget',
    'away_TeamLineUp',
    'home_TeamLineUp',
    'division'
    ]

game_df = game_df[cols_keep]
print(f"memory usage after deleting unessary columns:\t{game_df.memory_usage(index=True, deep=True).sum()/10**6} MB")
"""

sol_1_1 = """
game_df.away_TeamLineUp.str.contains("Philipp Lahm").sum() + game_df.home_TeamLineUp.str.contains("Philipp Lahm").sum()
"""
sol_1_2 = """
def count_occurrence_in_chuck(chunk):
    \"\"\" the function is given a dataframe and returns the number of game in which Pillip Lahm plays \"\"\"
    return chunk.away_TeamLineUp.str.contains("Philipp Lahm").sum() + chunk.home_TeamLineUp.str.contains("Philipp Lahm").sum()

iter_csv = pd.read_csv("data\inefficent_soccer_df.csv", iterator=True, chunksize=100) # Chunksize can be choosen according to available memory!
    
sum_games = 0    
for chunk in iter_csv:
    sum_games += count_occurrence_in_chuck(chunk)
    
print(f"Phillip Lahm played in {sum_games} games.")
"""

sol_2_1 = """
game_df.date = pd.to_datetime(game_df.date)
"""

sol_2_2 = """
(game_df.date.dt.weekday == 3).sum()
"""

sol_3 = """
game_df["acuracy"] = game_df.away_ShotsOnTarget / game_df.away_ShotsTotal

game_df\\
    .groupby("away_Team")\\
    .acuracy\\
    .mean()\\
    .sort_values()\\
    .head()
"""

sol_4 = """
game_df[game_df.home_Goal > game_df.away_Goal]\\
    .groupby("home_Team")\\
    .size()\\
    .sort_values(ascending=False)\\
    .head(5)
"""

sol_5 = """
game_df\\
    .groupby(game_df.date.dt.weekday_name)\\
    .mean()\\
    [["away_Goal","home_Goal"]]\\
    .sum(axis = 1)\\
    .sort_values()
"""


sol_6 = """
game_df\\
    .groupby("division")\\
    .mean()\\
    [['away_FoulsCommited', 'home_FoulsCommited']]\\
    .sum(axis = 1)\\
    .sort_values(ascending = False)
"""