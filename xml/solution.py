cabins = """
cabins = []
for passenger in passengers:
    cabin = passenger.find("Cabin")
    if cabin is not None:
        cabins.append(cabin.text)
cabins
"""

medical_jobs = """
import pandas as pd

medical_jobs_path = "data/medical_jobs.xml"
jobs = ET.parse(medical_jobs_path)
root = jobs.getroot()

data_with_contact = []

for page in root:
    for record in page:
        contact_person = record.find("contact_person")
        contact_phone_number = record.find("contact_phone_number")
        if contact_person is not None and contact_phone_number is not None:
            data_with_contact.append({
                'category': record.find("category").text,
                'company_name': record.find("company_name").text,
                'job_type': record.find("job_type").text,
                'contact_person': record.find("contact_person").text,
                'contact_phone_number': record.find("contact_phone_number").text,
                'location': record.find("location").text
            })
pd.DataFrame(data_with_contact)
"""