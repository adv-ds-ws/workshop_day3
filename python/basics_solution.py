change_city = """
adress['city'] = "Frankfurt"
adress
"""

welcome_each_day = """
for day_num in [1,2,3,4]:
    print(f"Hi there, welcome to day {day_num}")
"""

personal_welcome = """
def welcome_with_name(name, day_number):
    print(f"Hi {name}! Welcome to day {day_number}!")

welcome_with_name('Luke', 15)
"""

transfer_money = """
def transfer_money(accounts, account_number_from, account_number_to, amount):   
    from_account = accounts[account_number_from]
    to_account = accounts[account_number_to]
    if from_account['balance'] + from_account['credit'] - amount < 0:
        print('Transfer not possible! - Insufficiend funds!')
    else:
        from_account['balance'], to_account['balance'] = \\
            from_account['balance'] - amount, to_account['balance'] + amount
    return accounts
        
print("1. Transaction:")
accounts = transfer_money(accounts, '146284524', '943756323', 500)
print(accounts)
print("\\n2. Transaction:")
accounts = transfer_money(accounts, '943756323', '146284524', 500)
print(accounts)
"""

execute_transfers = """
def execute_transfers(accounts, transfers):
    for transfer in transfers:
        transfer_money(accounts, transfer['from'], transfer['to'], transfer['value'])
    return accounts

execute_transfers(accounts, transfers)
"""

add_interest = """
def add_interest(accounts, interest_rate):
    for account_number, account in accounts.items():
        if account['balance'] > 0:
            account['balance'] += account['balance'] * interest_rate
    return accounts
add_interest(copy.deepcopy(accounts), interest_rate=0.01)
"""

discount_credit_rate = """
def add_interest(accounts, interest_rate, credit_rate):
    for account_number, account in accounts.items():
        if account['balance'] > 0:
            account['balance'] += account['balance'] * interest_rate
        else:
            account['balance'] += account['balance'] * credit_rate
    return accounts
add_interest(copy.deepcopy(accounts), interest_rate=0.01, credit_rate=0.07)
"""

transfer_list = """
def transfer_money(accounts, account_number_from, account_number_to, amount):   
    from_account = accounts[account_number_from]
    to_account = accounts[account_number_to]
    if from_account['balance'] + from_account['credit'] - amount < 0:
        print('Transfer not possible! - Insufficiend funds!')
    else:
        from_account['balance'], to_account['balance'] = \
            from_account['balance'] - amount, to_account['balance'] + amount
        transfer = {'from': account_number_from, 'to': account_number_to, 'value': amount, 'time': datetime.datetime.now()}
        from_account['transfers'].append(transfer)
        to_account['transfers'].append(transfer)
    return accounts

def add_interest(accounts, interest_rate, credit_rate):
    for account_number, account in accounts.items():
        if account['balance'] > 0:
            amount = account['balance'] * interest_rate
            account['balance'] += amount
            transfer = {'from': 'interest', 'to': account_number, 'value': amount, 'time': datetime.datetime.now()}
        else:
            amount = account['balance'] * credit_rate
            account['balance'] += amount
            transfer = {'from': account_number, 'to': 'interest', 'value': amount, 'time': datetime.datetime.now()}
        account['transfers'].append(transfer)
    return accounts

transfer_money(copy.deepcopy(accounts), '943756323', '146284524', 500)
"""