street = """re.findall("([a-zA-Z]+\.?)\s\d+", signature)"""
number = """re.findall("[a-zA-Z]+\.?\s(\d+)", signature)"""
zip = """re.findall("\s(\d{5})\s\w+", signature)"""
city = """re.findall("\s\d{5}\s(\w+)", signature)"""

extract_wkn = """re.findall("WKN[\<\>\/\w]*([\dA-Z]{6})", txt)"""