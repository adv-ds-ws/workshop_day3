#!flask/bin/python

import datetime
from flask import Flask, jsonify, abort, make_response, request, url_for, render_template

app = Flask(__name__)

accounts = {'111222331': {'name': 'Max Müller',
                         'balance': 1247.87,
                         'credit': 500.00,
                         'transfers': []},
            '146284524': {'name': 'Katharina Kobald',
                        'balance': -44.85,
                        'credit': 150,
                         'transfers': []},
            '943756323': {'name': 'Susanne Jahn',
                         'balance': 58325.12,
                         'credit': 1000,
                         'transfers': []}}

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({"error":"Not found"}), 404)

@app.route("/", methods=["GET"])
def index():
    return render_template("home.html")

#TODO: Add a get_accounts() method

#TODO: Add a get_account() method to get a particular account.

@app.route("/transfer/<account_number_from>", methods=["PUT"])
def transfer(account_number_from):
    return jsonify({"error" : "Not Implemented"})
                                
def transfer_money(accounts, account_number_from, account_number_to, amount):   
    from_account = accounts[account_number_from]
    to_account = accounts[account_number_to]
    if from_account['balance'] + from_account['credit'] - amount < 0:
        print('Transfer not possible! - Insufficiend funds!')

    else:
        from_account['balance'], to_account['balance'] = from_account['balance'] - amount, to_account['balance'] + amount
        transfer = {'from': account_number_from, 'to': account_number_to, 'value': amount, 'time': datetime.datetime.now()}
        from_account['transfers'].append(transfer)
        to_account['transfers'].append(transfer)
    return accounts


@app.route("/interest", methods=["PUT"])
def interest():
    return jsonify({"error" : "Not Implemented"})

def add_interest(accounts, interest_rate, credit_rate):
    for account_number, account in accounts.items():
        if account['balance'] > 0:
            amount = account['balance'] * interest_rate
            account['balance'] += amount
            transfer = {'from': 'interest', 'to': account_number, 'value': amount, 'time': datetime.datetime.now()}
        else:
            amount = account['balance'] * credit_rate
            account['balance'] += amount
            transfer = {'from': account_number, 'to': 'interest', 'value': amount, 'time': datetime.datetime.now()}
        account['transfers'].append(transfer)

    return accounts

if __name__ == '__main__':
    app.run(debug=True)