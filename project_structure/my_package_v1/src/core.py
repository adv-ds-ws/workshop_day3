from . import helper

def greet_me(name="John Doe"):
    date = helper.get_date().strftime("%d-%b-%Y")
    print(f"Hello {name}. Today is {date}")