#### Calling / importing other code

# Relative import from your current working directory
import my_package_v1.src.core as mp_core
mp_core.greet_me()

# Absolute import after installing your package
# e.g. via
#   pip install my_package_v1/
import my_package.core as mp_core
mp_core.greet_me()

# But the user shouldn't know about the internal structure (core, etc)
# You can take care of that in the __init__.py of your package
import my_package as mp
mp.greet_me()
